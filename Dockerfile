FROM a7179072/bisai:v1.0.3
RUN mkdir /app
WORKDIR /app
ADD /app/ /app/

EXPOSE 5000
CMD ["python", "/app/main.py"]