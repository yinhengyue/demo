本项目为EKS 发部python-flask集群项目
Jenkins创建在EKS中，使用动态slave+autoscaler模式，构建完成之后自动销毁所有使用的资源包括EC2
使用共享库配置，多项目共用一个共享库
项目分两步CI,CD  
CI项目为一次构建，提审升级的方式，目前只做了一个测试环境发版，提审升级为手动:

- gitlab打tag
- ​创建测试环境docker镜像tag为：test-gitcommitid-gittagname
- ​qa测试成功后
- 升级为预发环境，同一个镜像添加tag：stage-gitcommitid-gittagname
- ​qa测试成功后
- ​升级为生产环境，同一个镜像添加tag：prod-gitcommitid-gittagname

CD（CD发布到eks中会有暴露accesskey的风险所以少公开了一个带有kubectl的docker镜像，不过流水线的代码是对的(注释了)）:

- ​测试环境发版通过docker-api匹配test-*的镜像
- 预发环境发版通过docker-api匹配stage-*的镜像
- 生产环境发版通过docker-api匹配prod-*的镜像